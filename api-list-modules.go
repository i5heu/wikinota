package main

import (
	"database/sql"
	"errors"
	"regexp"
)

func ListModules(jsonreq ReqStruct) (ids *sql.Rows, err error) {

	var jsondata map[string]interface{} = jsonreq.DATA

	DebugOutput("ListModules - ListModule:", jsondata["ListModule"].(string))

	switch jsondata["ListModule"].(string) {
	case "Path":
		selector := "Path"
		if transitionerr, ok := jsondata[selector].(string); ok { //chek if Selectet object can be transcoedet to strint
			DebugOutput("Listmoduel:Path:ERROR: ", err, ids, " SELECTOR:", jsondata[selector].(string), " Converter:", transitionerr)
			ids, err = db.Query(sqlc.ListByPath, NameToID(jsonreq.UserName), jsondata[selector].(string))
		} else {
			DebugOutput("Listmoduel:Path:ERROR: FAILED", transitionerr)
		}
	case "PathHierarchy":
		selector := "Path"
		if _, ok := jsondata[selector].(string); ok {
			pathtmp := jsondata[selector].(string) + "%"
			DebugOutput("PathHierarchy PATH:", pathtmp)
			ids, err = db.Query(sqlc.ListByPathHierarchy, NameToID(jsonreq.UserName), pathtmp)
		}
	case "PathMATCH":
		selector := "Path"
		if _, ok := jsondata[selector].(string); ok {
			ids, err = db.Query(sqlc.ListByPathMATCH, jsondata[selector].(string), jsondata[selector].(string), NameToID(jsonreq.UserName))
		}
	case "ListApp":
		selector := "APP"
		if _, ok := jsondata[selector].(string); ok {
			ids, err = db.Query(sqlc.ListByApp, NameToID(jsonreq.UserName), jsondata[selector].(string))
		}
	case "ListFullSearch":
		selector := "Searchterm"
		if transitionerr, ok := jsondata[selector].(string); ok {
			ids, err = db.Query(sqlc.ListFullSearch, jsondata[selector].(string), jsondata[selector].(string), NameToID(jsonreq.UserName))
			DebugOutput("Listmoduel:ListFullSearch:Query - ", err)
		} else {
			DebugOutput("Listmoduel:ListFullSearch:ERROR: FAILED", transitionerr)
		}
	case "ListInstantSearch":
		selector := "Searchterm"
		if transitionerr, ok := jsondata[selector].(string); ok {
			ids, err = db.Query(sqlc.ListInstantSearch, jsondata[selector].(string), jsondata[selector].(string), NameToID(jsonreq.UserName))
			DebugOutput("Listmoduel:ListInstantSearch:Query - ", err)
		} else {
			DebugOutput("Listmoduel:ListInstantSearch:ERROR: FAILED", transitionerr)
		}
	case "ListArticleDesktop":
		ids, err = db.Query(sqlc.ListArticleDesktop, NameToID(jsonreq.UserName))
	case "ListPathSubSection":
		selector := "Path"
		if _, ok := jsondata[selector].(string); ok {
			ifdublepoint := regexp.MustCompile(":")
			matches := ifdublepoint.FindAllStringIndex(jsondata[selector].(string), -1)
			ids, err = db.Query(sqlc.ListPathSubSection, len(matches)+2, len(matches)+1, NameToID(jsonreq.UserName), jsondata[selector].(string))
		}
	case "ListPathMainSection":
		ids, err = db.Query(sqlc.ListPathMainSection, NameToID(jsonreq.UserName))
	case "GeldlogAll":
		ids, err = db.Query(sqlc.GeldlogAll)
	default:
		DebugOutput("ListModule: No Valid API USAGE")
		err = errors.New("ListModule: No Valid API USAGE")
	}

	return
}
