package main

import (
	"database/sql"
	"errors"
	"fmt"
	"reflect"
	"regexp"
	"strings"
	"time"
)

type ASimpleStatus struct {
	Status string
}

//AError is an help struct for AnswerStruct
type AError struct {
	Error string
}

//ErrorAPIBuilder is generating an Error Struct for the AnswerStruct
func ErrorAPIBuilder(err error, res *ResStruct) {
	if err == nil {
		return
	}

	apierr := AError{
		Error: err.Error(),
	}

	fmt.Println("ERROR-", err.Error())
	res.Error = true
	res.DATA = apierr

	return
}

//BackupItem creates an backup in BUItems from selectet Item
func BackupItem(db *sql.DB, UserId int, Path string) (err error) {

	_, err = db.Exec(sqlc.ItemBackuper, UserId, Path)
	DebugOutput("BackUp Item with Path:", Path, " Error:", err)

	return
}

//CreatePath creates the Path for mysql path
func CreatePath(Pathname string, APP string) (out string, err error) {

	if strings.Index(APP, ":") != -1 {
		err = errors.New("APP contains ':'")
		return
	}

	if Pathname == "" || Pathname == ":" {
		TimeTmp := string(time.Now().Format(time.RFC822))
		Pathname = "TIME-" + strings.Replace(TimeTmp, ":", "-", -1)
	}

	Pathname = strings.Replace(Pathname, " ", "_", -1)
	Pathname = strings.Replace(Pathname, "*", "_", -1)
	chekForRedundantUnderscore := regexp.MustCompile(`[_]{2,}`)
	Pathname = chekForRedundantUnderscore.ReplaceAllString(Pathname, "_")

	chekForRedundantDublepointsRegx := regexp.MustCompile(`[:]{2,}`)

	Pathname = chekForRedundantDublepointsRegx.ReplaceAllString(Pathname, ":")

	if Pathname[0:1] == ":" {
		Pathname = Pathname[1:len(Pathname)]
	}

	tmp := SplitPathStringToStringArray(Pathname)

	if APP == "" || APP == ":" {
		APP = "VOID"
	}

	if tmp[0] != APP {
		out = APP + ":" + Pathname
	} else {
		out = Pathname
	}
	return
}

func SplitPathStringToStringArray(in string) (out []string) {
	out = strings.Split(in, ":")
	return
}

//InterfaceNilIntChek cheks if the interface is able to convert to the type of neededType
func InterfaceNilChek(jsonres *ResStruct, jsonreq ReqStruct, selector string, neededType string) (out interface{}) {

	//This overjumps the heavy work in case of an error - the error is send anyway
	if jsonres.Error != false {
		switch neededType {
		case "string":
			out = ""
		case "JsonString":
			out = "{}"
		case "int":
			out = 0
		case "float64":
			out = 0.00
		case "bool":
			out = false

		}
		DebugOutput("JumpOver InterfaceNilChek because of Error")
		return
	}

	//fill if the jsonreq.DATA[selector] is nil and give return
	if jsonreq.DATA[selector] == nil {

		DebugOutput("InterfaceNilChek Fill Nil of --", selector)
		switch neededType {
		case "string":
			out = "NO-DATA"
			return
		case "JsonString":
			out = "{}"
			return
		case "bool":
			out = false
			return
		case "int", "float64":
			out = 0.00
			return
		default:
			out = 0
			return
		}
	}

	//chek if the interface can transtypet
	switch neededType {
	case "string", "JsonString":
		if err, ok := jsonreq.DATA[selector].(string); ok {
			out = jsonreq.DATA[selector]
		} else {
			jsonres.Error = true
			out = "ERROR:" + err
		}
	case "bool":
		if _, ok := jsonreq.DATA[selector].(bool); ok {
			out = jsonreq.DATA[selector]
		} else {
			jsonres.Error = true
			out = false
		}
	case "float64", "int":
		if _, ok := jsonreq.DATA[selector].(float64); ok {
			if neededType == "int" {
				out = int(jsonreq.DATA[selector].(float64))
			} else {
				out = jsonreq.DATA[selector]
			}
		} else {
			jsonres.Error = true
			out = 0
		}
	default:
		errortext := selector + " is not in valid type"
		jsonres.Error = true
		jsonres.DATA = map[string]interface{}{
			"ERROR": errortext,
		}
		out = 0
	}

	if jsonres.Error != false && jsonres.DATA == nil {
		errortext := selector + " is not a " + neededType + " or is empty"
		jsonres.Error = true
		jsonres.DATA = map[string]interface{}{
			"ERROR": errortext,
		}

		return
	}
	DebugOutput("InterfaceNilChek Selector:", selector, " by type:", reflect.TypeOf(jsonreq.DATA[selector]))
	return
}

func ChekIfPathExistWithUsername(path string, USERID int) (exists bool) {

	err := db.QueryRow(sqlc.ChekIfPathExistWithUsername, USERID, path).Scan(&exists)

	if err != nil {
		fmt.Println("ERROR ERROR EROOR THERE SHOLD BE NO ERROR IN FUNC ChekIfPathExistWithUsername->", err.Error())
	}

	return
}
