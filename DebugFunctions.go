package main

import (
	"fmt"
	"io/ioutil"
	"time"
)

func DebugInitiator() {

	go func() {
		var err error
		for {
			time.Sleep(5 * time.Second)

			indexCache, err = ioutil.ReadFile("./webapp/index.html") // THIS CAN BE A GLOBAL VAR (CACHING)
			if err != nil {
				fmt.Println(err)
			}

			ServiceWorkerCache, err = ioutil.ReadFile("./webapp/js/pwabuilder-sw.js") // THIS CAN BE A GLOBAL VAR (CACHING)
			if err != nil {
				fmt.Println(err)
			}

		}
	}()

}
