package main

import (
	"errors"
	"fmt"
	"time"
)

type ReqStruct struct {
	UserName string
	PWD      string
	Method   string
	DATA     map[string]interface{}
}

//AnswerStruct
type ResStruct struct {
	Error            bool
	ElapsedLogicTime time.Duration //This is the
	DATA             interface{}   `json:"DATA"`
}

//Handler is the main API handler - it directs requests to the right function
func Handler(jsonreq ReqStruct) (jsonres ResStruct) {

	fmt.Println("LOGIN ATEMPT--", jsonreq.UserName, "--", authenticator(jsonreq.UserName, jsonreq.PWD))

	if authenticator(jsonreq.UserName, jsonreq.PWD) == true { // this functions need user authentication
		switch jsonreq.Method {
		case "list":
			jsonres = list(jsonreq)
		case "create_item":
			jsonres = create_item(jsonreq)
		case "delete_item":
			jsonres = delete_item(jsonreq)
		default:
			ErrorAPIBuilder(errors.New("NO METHOD"), &jsonres)
		}
	} else {
		ErrorAPIBuilder(errors.New("authentication failed"), &jsonres)
	}

	return
}
