package main

type sqlcomands struct {
	CreateItems     string
	CreateEventlog  string
	CreateBUarticle string
	CreateBUitems   string
	CreateUsers     string

	ApiItemWrite  string
	ApiItemUpdate string
	ApiItemDelete string

	ListByPath          string
	ListByPathHierarchy string
	ListByPathMATCH     string
	ListFullSearch      string
	ListInstantSearch   string
	ListByApp           string
	ListArticleDesktop  string

	ItemBackuper string

	ListPathSubSection  string
	ListPathMainSection string

	GeldlogAll string

	Authenticator_gethashes     string
	ChekIfPathExistWithUsername string
}

var sqlc sqlcomands

func FillSqlComands() { // using an Function for easy migrating to multi DB
	sqlc = sqlcomands{

		CreateItems: "CREATE TABLE `items` ( `ItemID` int(10) unsigned NOT NULL AUTO_INCREMENT, `UserId` int(10) unsigned NOT NULL, `path` varchar(500) NOT NULL, `timecreate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP, `timelastedit` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP, `public` tinyint(1) NOT NULL DEFAULT '0', `APP` varchar(20) NOT NULL DEFAULT 'NONE', `viewcounter` int(11) NOT NULL DEFAULT '0', `editcounter` int(11) NOT NULL DEFAULT '0', `title1` varchar(128) NOT NULL DEFAULT 'main', `title2` varchar(128) NOT NULL DEFAULT 'main', `text1` text NOT NULL, `text2` text NOT NULL, `tags1` varchar(256) NOT NULL DEFAULT ' ', `num1` decimal(64,2) NOT NULL DEFAULT '0.00', `num2` decimal(64,2) NOT NULL DEFAULT '0.00', `num3` int(64) NOT NULL DEFAULT '0', `NoSqlData` json DEFAULT NULL, PRIMARY KEY (`ItemID`), KEY `path` (`path`), KEY `UserIndex` (`UserId`), KEY `PathAcces` (`UserId`,`path`), FULLTEXT KEY `FULLTEXT` (`path`,`title1`,`title2`,`text1`,`text2`,`tags1`) ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1 ",

		CreateEventlog: "CREATE TABLE `eventlog` ( `id` int(11) NOT NULL AUTO_INCREMENT, `UserId` int(10) unsigned NOT NULL, `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP, `description` varchar(500) NOT NULL, PRIMARY KEY (`id`) ) ENGINE=InnoDB DEFAULT CHARSET=latin1",

		CreateBUitems: "CREATE TABLE `BUitems` ( `BUitemID` int(10) unsigned NOT NULL AUTO_INCREMENT, `ItemID` int(10) unsigned NOT NULL, `UserId` int(10) unsigned NOT NULL, `path` varchar(500) NOT NULL, `timecreate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP, `timelastedit` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP, `public` tinyint(1) NOT NULL DEFAULT '0', `APP` varchar(20) NOT NULL DEFAULT 'NONE', `viewcounter` int(11) NOT NULL DEFAULT '0', `editcounter` int(11) NOT NULL DEFAULT '0', `title1` varchar(128) NOT NULL DEFAULT 'main', `title2` varchar(128) NOT NULL DEFAULT 'main', `text1` text NOT NULL, `text2` text NOT NULL, `tags1` varchar(256) NOT NULL DEFAULT ' ', `num1` decimal(64,2) NOT NULL DEFAULT '0.00', `num2` decimal(64,2) NOT NULL DEFAULT '0.00', `num3` int(64) NOT NULL DEFAULT '0', `NoSqlData` json DEFAULT NULL, PRIMARY KEY (`BUitemID`) ) ENGINE=InnoDB DEFAULT CHARSET=latin1",

		CreateUsers: "CREATE TABLE `users` ( `UserId` int(10) unsigned NOT NULL AUTO_INCREMENT, `username` varchar(300) NOT NULL, `userhash` char(128) NOT NULL, `email` varchar(300) NOT NULL, `createTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP, `comment` varchar(1000) NOT NULL DEFAULT 'none', PRIMARY KEY (`UserId`), UNIQUE KEY `UserId` (`UserId`), UNIQUE KEY `email` (`email`), KEY `username` (`username`) ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1 ",

		ApiItemWrite:  "INSERT INTO items(UserId,path,APP,title1,title2,text1,text2,tags1,num1,num2,num3,public,NoSqlData) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)",
		ApiItemUpdate: "UPDATE items SET APP=?,title1=?,title2=?,text1=?,text2=?,tags1=?,num1=?,num2=?,num3=?,public=?,NoSqlData=? WHERE UserID = ? AND path = ?",
		ApiItemDelete: "DELETE FROM items WHERE UserID = ? AND path = ?",

		ListByPath:          "SELECT path,timecreate,timelastedit,APP,title1,title2,text1,text2,tags1,num1,num2,num3,NoSqlData,public from items WHERE UserID = ? AND path = ?",
		ListByPathHierarchy: "SELECT path,timecreate,timelastedit,APP,title1,title2,text1,text2,tags1,num1,num2,num3,NoSqlData,public FROM items WHERE UserID = ? AND path LIKE ? LIMIT 300",
		ListFullSearch:      "SELECT path,timecreate,timelastedit,APP,title1,title2,text1,text2,tags1,num1,num2,num3,NoSqlData,public, MATCH (path,title1,title2,text1,text2,tags1) AGAINST (? IN BOOLEAN MODE) AS score FROM items WHERE MATCH (path,title1,title2,text1,text2,tags1) AGAINST (? IN BOOLEAN MODE) AND (public = 0 OR public = 1 ) AND UserID = ?  ORDER BY score DESC LIMIT 300",
		ListInstantSearch:   "SELECT path,timecreate,timelastedit,APP,title1,title2,LEFT(text1,100) as text1 ,LEFT(text2,100) as text2 ,tags1,num1,num2,num3,NoSqlData,public, MATCH (path,title1,title2,text1,text2,tags1) AGAINST (? IN BOOLEAN MODE) AS score FROM items WHERE MATCH (path,title1,title2,text1,text2,tags1) AGAINST (? IN BOOLEAN MODE) AND (public = 0 OR public = 1 ) AND UserID = ?  ORDER BY score DESC LIMIT 10",
		ListByPathMATCH:     "SELECT path,timecreate,timelastedit,APP,title1,title2,text1,text2,tags1,num1,num2,num3,NoSqlData,public, MATCH (path) AGAINST (? IN BOOLEAN MODE) AS score FROM items WHERE MATCH (path) AGAINST (? IN BOOLEAN MODE) AND (public = 0 OR public = 1 ) AND UserID = ? ORDER BY score DESC LIMIT 300",
		ListByApp:           "SELECT path,timecreate,timelastedit,APP,title1,title2,LEFT(text1,300),LEFT(text2,300),tags1,num1,num2,num3,NoSqlData,public FROM items WHERE UserID = ? AND APP = ? ORDER BY timelastedit DESC LIMIT 300",

		ListArticleDesktop: "SELECT path,timecreate,timelastedit,APP,title1,title2,text1,text2,tags1,num1,num2,num3,NoSqlData,public from items WHERE UserID = ? ORDER BY timelastedit DESC LIMIT 300",

		ListPathSubSection:  "SELECT DISTINCT(SUBSTRING_INDEX(PATH, ':', ?)) path, SUBSTRING_INDEX(PATH, ':',?) sections FROM items  WHERE UserID = ? HAVING sections = ? LIMIT 5000",
		ListPathMainSection: "SELECT DISTINCT(SUBSTRING_INDEX(path, ':', 1)) path FROM items WHERE UserID = ? LIMIT 300",

		ItemBackuper: "INSERT INTO BUitems(`ItemID`,`UserID`,`path`, `timecreate`, `timelastedit`, `public`, `APP`, `viewcounter`, `editcounter`, `title1`, `title2`, `text1`, `text2`, `tags1`, `num1`, `num2`, `num3`, `NoSqlData`)SELECT `ItemID`,`UserID`, `path`, `timecreate`, `timelastedit`, `public`, `APP`, `viewcounter`, `editcounter`, `title1`, `title2`, `text1`, `text2`, `tags1`, `num1`, `num2`, `num3`, `NoSqlData` FROM items WHERE UserID = ? AND Path = ?",

		Authenticator_gethashes: "SELECT UserId,username,userhash FROM `users`",

		ChekIfPathExistWithUsername: "SELECT EXISTS(SELECT 1 FROM items WHERE UserId = ? AND path = ? )",
	}
}
